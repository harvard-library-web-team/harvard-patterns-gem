# Harvard Patterns Gem

This repo contains a Ruby Gem with built assets from Harvard Patterns.

Loosely based on these tutorials:
* [Wrap Your Assets In A Gem](https://medium.com/@paulfarino/wrap-your-assets-in-a-gem-3ad7ecf5b075)
* [Publishing Your Gem](https://guides.rubygems.org/publishing/)